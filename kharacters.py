import csv

def find_all_characters(filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        data = list(reader) 
    return data if len(data) > 0 else []

def find_character_by_id(filename, character_id):
    characters = find_all_characters(filename)
    for character in characters:
        if character['id'] == str(character_id):
            return character 
    return None

def create_character(filename, **kwargs):
    fieldnames = ['id', 'name', 'intelligence', 'power', 'strength', 'agility']
    heroes = []

    with open(filename, 'r', newline='') as csvfile:
        reader = list(csv.DictReader(csvfile))
        new_char_id = int(reader[-1]['id']) + 1 if int(reader[-1]['id']) else 1
        for hero in reader:
            heroes.append(hero)
    
    new_char = {'id': new_char_id, **kwargs}
    heroes.append(new_char)

    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(heroes)
    
    return new_char